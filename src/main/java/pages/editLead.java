package pages;

import wrappers.Basepage;

public class editLead extends Basepage {
	
	public viewlead editlead()
	{
		driver.findElementByXPath("(//a[text()='Edit'])").click();
        driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").clear();
        driver.findElementByXPath("//input[@id='updateLeadForm_companyName']").sendKeys("CTS");
        driver.findElementByXPath("//input[@name='submitButton' and @value='Update']").click();
          driver.findElementByXPath("//span[@id='viewLead_companyName_sp']");  
          return new viewlead();
		
	}

}

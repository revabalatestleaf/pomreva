package pages;

import org.testng.annotations.Test;

import wrappers.Basepage;

public class CreateLeadPage extends Basepage {
	
	public viewlead createlead()
	{
		driver.findElementByXPath("(//ul[@class='shortcuts']//ancestor::a)[1]").click();
		driver.findElementByXPath("//input[@id='createLeadForm_firstName']").sendKeys("Reva");
		driver.findElementByXPath("//input[@id='createLeadForm_lastName']").sendKeys("Bala");
		driver.findElementByXPath("//input[@id='createLeadForm_companyName']").sendKeys("TCS");
		driver.findElementByXPath("//input[@name='submitButton']").click();
		return new viewlead();
		
 
	}
	

}
